<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'news'      => 'Новости',
            'ether'     => 'Эфиры',
            'projects'  => 'Проекты',
            'feedback'  => 'Обратная связь',
            'announce'  => 'Анонсы',
            'online'    => 'Прямой эфир (online)'
        ];
        foreach($data as $code => $title) {
            \App\Category::create([
                'code' => $code,
                'title' => $title,
                'status' => 'show'
            ]);
        }
    }
}
