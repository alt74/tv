<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->words(3, true)
    ];
});

$factory->define(App\Post::class, function ($faker) {
    return [
        'title' => $faker->words(7, true),
        'brief' => $faker->paragraph,
        'content' => $faker->text,
        'status' => $faker->randomKey(\App\Repositories\PostBaseRepository::statuses()),
        'date' => $faker->dateTime(),
        'date_begin' => $faker->dateTimeBetween('-1 years'),
        'date_end' => $faker->dateTimeBetween('+1 years', '+2 years'),
        'priority' => $faker->randomKey(\App\Repositories\PostBaseRepository::priorities()),
        'category_id' => function () {
            return App\Category::inRandomOrder()->first()->id;
        },
        'author_id' => function () {
            return App\Author::inRandomOrder()->first()->id;
        },
        'source_id' => function () {
            return App\Source::inRandomOrder()->first()->id;
        },
    ];
});
