<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSomeImageFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('path');
            $table->dropColumn('thumbnail_path');
            $table->dropColumn('ico_path');
            $table->dropColumn('position');
            $table->dropColumn('image_width');
            $table->dropColumn('image_height');
            $table->dropColumn('thumbnail_width');
            $table->dropColumn('thumbnail_height');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->string('path');
            $table->string('thumbnail_path');
            $table->string('ico_path');
            $table->string('position');
            $table->integer('image_width')->default(0);
            $table->integer('image_height')->default(0);
            $table->integer('thumbnail_width')->default(0);
            $table->integer('thumbnail_height')->default(0);

        });
    }
}
