<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('type');
            $table->string('videokey');
            $table->string('image_path');
            $table->string('image_width');
            $table->string('image_height');
            $table->string('quality');

            $table->timestamps();
        });

        Schema::create('video2tag', function(Blueprint $table){
            $table->integer('video_id')
                ->unsigned()
                ->index();
            $table->foreign('video_id')
                ->references('id')
                ->on('videos')
                ->onDelete('cascade');

            $table->integer('tag_id')
                ->unsigned()
                ->index();
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
            $table->string('status');
            $table->integer('priority');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
        Schema::drop('video2tag');
    }
}
