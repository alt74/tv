<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInfoToVideoTable extends Migration
{

    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->text('info')
                ->after('url')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn('info');
        });
    }
}
