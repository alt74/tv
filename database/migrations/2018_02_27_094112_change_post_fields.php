<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePostFields extends Migration
{

    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->datetime('date_begin')->change();
            $table->datetime('date_end')->change();
        });
    }

    public function down()
    {
        //
    }
}
