<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->string('title');
            $table->text('brief');
            $table->text('content');
            $table->string('status');
            $table->date('date_begin')->nullable();
            $table->date('date_end')->nullable();
            $table->integer('priority');
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('author_id')->unsigned()->nullable();
            $table->integer('source_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->foreign('author_id')
                ->references('id')
                ->on('authors')
                ->onDelete('cascade');

            $table->foreign('source_id')
                ->references('id')
                ->on('sources')
                ->onDelete('cascade');

        });

        Schema::create('post2tag', function(Blueprint $table){
            $table->integer('post_id')
                ->unsigned()
                ->index();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');

            $table->integer('tag_id')
                ->unsigned()
                ->index();
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('post2image', function(Blueprint $table){
            $table->integer('post_id')
                ->unsigned()
                ->index();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');

            $table->integer('image_id')
                ->unsigned()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('images')
                ->onDelete('cascade');
            $table->string('status');
            $table->string('position');
            $table->integer('priority');

            $table->timestamps();
        });

        Schema::create('post2video', function(Blueprint $table){
            $table->integer('post_id')
                ->unsigned()
                ->index();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');

            $table->integer('video_id')
                ->unsigned()
                ->index();
            $table->foreign('video_id')
                ->references('id')
                ->on('videos')
                ->onDelete('cascade');
            $table->string('status');
            $table->string('position');
            $table->integer('priority');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
        Schema::drop('post2tag');
        Schema::drop('post2image');
        Schema::drop('post2video');
    }
}
