<div class="form-group {{ $errors->has($fieldname_date) ? ' is-invalid' : ' is-valid'}}">
    <label class="control-label" for="date-{{ $fieldname_date }}">{{ $fieldtitle }}</label>
    <div class="input-group">
        <input
                type="text"
                class="form-control"
                id="{{ $fieldname_date }}"
                name="{{ $fieldname_date }}"
                value="{{ old($fieldname_date, (isset($info->$fieldname_date) ? $info->$fieldname_date : '')) }}"
                aria-describedby="{{ $fieldname_date }}_status"
                placeholder="dd.mm.yyyy"
        />
        <input
                type="text"
                class="form-control"
                id="{{ $fieldname_time }}"
                name="{{ $fieldname_time }}"
                style="width: 100px;"
                value="{{ old($fieldname_time, (isset($info->$fieldname_time) ? $info->$fieldname_time : '')) }}"
                placeholder="HH:mm"
        />
    </div>
    @if ($errors->has($fieldname_date) || $errors->has($fieldname_time))
        <span id="{{ $fieldname_date }}_status" class="invalid-feedback">
            @if ($errors->has($fieldname_date))
            <strong>{{ $errors->first($fieldname_date) }}</strong>
            @endif
            @if ($errors->has($fieldname_time))
            <strong>{{ $errors->first($fieldname_time) }}</strong>
            @endif
        </span>
    @endif
</div>
