@extends('layouts.simple')

@section('content')

    <form role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}

        <h1 class="h3 mb-3 font-weight-normal">Sign Up</h1>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="form-group">
            <label for="in-name">Name</label>
            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '  is-valid' }}" id="in-name" aria-describedby="nameHelp" placeholder="Enter name" autofocus name="name" value="{{ old('name') }}">
            @if ($errors->has('name'))
                <span id="nameHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-email">Email address</label>
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '  is-valid' }}" id="in-email" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{ old('email') }}">
            @if ($errors->has('email'))
                <span id="emailHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-password">Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-password" placeholder="Password" name="password" aria-describedby="passwordHelp">
            @if ($errors->has('email'))
                <span id="passwordHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-confirm-password">Confirm Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-confirm-password" placeholder="Confirm Password" name="password_confirmation" aria-describedby="passwordConfirmationHelp">
            @if ($errors->has('password_confirmation'))
                <span id="password_confirmation" class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Sign Up</button>
            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
            <a class="btn btn-link" href="{{ url('/login') }}">Sign In</a>
        </div>

    </form>

@endsection
