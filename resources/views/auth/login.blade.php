@extends('layouts.simple')

@section('content')

    <form role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}

        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

        <div class="form-group">
            <label for="in-email">Email address</label>
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '  is-valid' }}" id="in-email" aria-describedby="emailHelp" placeholder="Enter email" autofocus name="email" value="{{ old('email') }}">
            @if ($errors->has('email'))
                <span id="emailHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-password">Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-password" placeholder="Password" name="password" aria-describedby="passwordHelp">
            @if ($errors->has('email'))
                <span id="passwordHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="remember-me" name="remember">
            <label class="form-check-label" for="remember-me">Remember me</label>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
            <a class="btn btn-link" href="{{ url('/register') }}">Sign Up</a>
        </div>

    </form>

@endsection
