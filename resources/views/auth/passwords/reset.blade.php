@extends('layouts.simple')

@section('content')

    <form role="form" method="POST" action="{{ url('/password/reset') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">

        <h1 class="h3 mb-3 font-weight-normal">Reset Password</h1>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="form-group">
            <label for="in-email">Email address</label>
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '  is-valid' }}" id="in-email" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{ $email or old('email') }}">
            @if ($errors->has('email'))
                <span id="emailHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-password">Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-password" placeholder="Password" name="password" aria-describedby="passwordHelp">
            @if ($errors->has('email'))
                <span id="passwordHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-confirm-password">Confirm Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-confirm-password" placeholder="Confirm Password" name="password_confirmation" aria-describedby="passwordConfirmationHelp">
            @if ($errors->has('password_confirmation'))
                <span id="password_confirmation" class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Reset Password</button>
        </div>

    </form>

@endsection
