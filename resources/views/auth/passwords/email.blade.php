@extends('layouts.simple')

@section('content')

    <form role="form" method="POST" action="{{ url('/password/email') }}">
        {{ csrf_field() }}

        <h1 class="h3 mb-3 font-weight-normal">Reset Password</h1>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="form-group">
            <label for="in-email">Email address</label>
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '  is-valid' }}" id="in-email" aria-describedby="emailHelp" placeholder="Enter email" autofocus name="email" value="{{ old('email') }}">
            @if ($errors->has('email'))
                <span id="emailHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Send Password Reset Link</button>
            <a class="btn btn-link" href="{{ url('/login') }}">Sign In</a>
            <a class="btn btn-link" href="{{ url('/register') }}">Sign Up</a>
        </div>

    </form>

@endsection
