<!-- LATEST -->
{{--<h3 class="page-header fw-300 mt-60">--}}
{{--Read <span>this</span>, Watch <span>that</span>--}}
{{--</h3>--}}

<div id="accordion">
    <div class="card card-default mt-5 mb-0">
        <div class="card-heading">
            <h4 class="card-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <i class="fa fa-heart-o"></i>
                    Most readed
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="accordion-body collapse in">
            <div class="card-block">
                <ul class="list-unstyled list-icons mb-10">
                    <li class="mt-6"><i class="fa fa-angle-right"></i> <a href="#">Boulder smashes through Italian farm</a></li>
                    <li class="mt-6"><i class="fa fa-angle-right"></i> <a href="#">Fall in eurozone inflation rate fuels deflation concerns</a></li>
                    <li class="mt-6"><i class="fa fa-angle-right"></i> <a href="#">Australia to dump dredged sand in Great Barrier Reef Park</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
