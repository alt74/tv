<div class="col-6 col-md-6">
    <a href="{{ url('post/'.$post->id) }}">
        <img alt="" class="img-fluid" src="{{ $post->thumbnail }}" />
        <h6 class="fsize12 font300 padding6">{{ $post->title }}</h6>
    </a>
</div>