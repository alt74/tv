@extends('layouts.app')

@section('content')
    <div class="item-box">
        <div class="item-box-desc">
            <div class="mt-30 mb-30">
                <h4><a href="{{ url('post/'.$post->id) }}">{{ $post->title }}</a></h4>
                <small>@datetime($post->date)</small>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="class-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                {!! $post->media[0] !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    {{ $post->brief }}
                </div>
            </div>
            <p>{{ $post->content }}</p>
        </div>
    </div>
@endsection

@section('right')
    @include('post._vertical_list')
    @include('post._statistics')
@endsection

@section('script')

@endsection
