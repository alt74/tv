<!-- article item -->
<div class="item-box">
    <figure>
        <a class="item-hover" href="{{ url('post/'.$post->id) }}">
            <span class="overlay color2"></span>
            <span class="inner">
                <span class="block fa fa-plus fsize20"></span>
                Читать новость
            </span>
        </a>
        <img alt="" class="img-fluid" src="{{ $post->thumbnail }}"  />
    </figure>
    <div class="item-box-desc">
        <h4><a href="{{ url('post/'.$post->id) }}">{{ $post->title }}</a></h4>
        <small>@datetime($post->date)</small>
        <p>{{ $post->brief }}</p>
    </div>
</div>
<!-- /article item -->
