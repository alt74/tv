@if ($block_title)
<h3 class="page-header fw-300 mt-30"><a href="#" data-toggle="tooltip" title="view more"><i class="fa fa-plus-square-o"></i></a>
    {{ $block_title }} @if(isset($block_title_sel))<strong>{{ $block_title_sel }}</strong>@endif
</h3>
@endif

<div class="mt-30 mb-30">
    @if(isset($block_description)){{ $block_description }}@endif
</div>

@foreach ($list->chunk(3) as $chunk)
<div class="row mt-50">
    @foreach ($chunk as $post)
    <div class="col-md-4">
        @include('post._table_item_one', ['post' => $post])
    </div>
    @endforeach
</div>
@endforeach

@include('pagination.default', ['paginator' => $list])

