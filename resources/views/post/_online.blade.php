        <!-- TWO COLUMNS -->
        <div class="row">

            <!-- first column -->
            <div class="col-md-12">

                <h3 class="page-header fw-300">
                    TV Online
                </h3>

                <div class="card">
                    <div class="card-body">
                        <div class="embed-responsive embed-responsive-16by9" id="player">

                        </div>
                        <p class="card-text mt-4"></p>
                    </div>
                </div>

            </div>

        </div>
        <!-- /TWO COLUMNS -->
<!-- PlayerJS -->
<script src="https://oriontv.net/playerjs.js"></script>
<script>
    var player = new Playerjs({id: "player", file: "https://oriontv.net/auto.m3u8", poster: "https://oriontv.net/logo.png"});
</script>