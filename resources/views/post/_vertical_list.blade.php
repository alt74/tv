@if (!empty($lists[0]))
    <h3 class="page-header mt-0 fw-300">
        {{ $lists[0]['category']->title }}
    </h3>
    @foreach($lists[0]['list'] as $post)
        @include('post._table_item_two', ['post' => $post])
    @endforeach
@endif

<!-- small articles -->
<div class="row mt-30">
    @if (!empty($lists[1]))
        @foreach($lists[1]['list'] as $post)
            @include('post._table_item_small', ['post' => $post])
        @endforeach
    @endif
</div>
<!-- /small articles -->
