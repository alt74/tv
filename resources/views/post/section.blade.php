@extends('layouts.app')

@section('content')
    @include('post._table_list', [
    'list' => $list,
    'block_title' => $category->title,
    'block_description' => $category->description
    ])
@endsection

@section('right')
    @include('post._vertical_list')
    @include('post._statistics')
@endsection

@section('script')

@endsection
