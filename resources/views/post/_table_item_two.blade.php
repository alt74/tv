<div class="item-box mt-0">
    <figure>
        <a class="item-hover" href="{{ url('post/'.$post->id) }}">
            <span class="overlay color2"></span>
										<span class="inner">
											<span class="block fa fa-plus fsize20"></span>
											Читать
										</span>
        </a>
        <img alt="" class="img-fluid" src="{{ $post->thumbnail }}" />
    </figure>
    <div class="item-box-desc p-10">
        <small>@datetime($post->date)</small>
        <h4><a href="{{ url('post/'.$post->id) }}">{{ $post->title }}</a></h4>
    </div>
</div>