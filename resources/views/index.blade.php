@extends('layouts.app')

@section('content')
    <div class="text-center">
        {{--<img src="{{ url('banners/top.png') }}" />--}}
    </div>
    @if ($is_online_on)
        @include('post._online', ['list' => $online, 'block_title' => 'Прямой эфир'])
    @endif
    @include('post._table_list', ['list' => $list, 'block_title' => ''])

    <div id="catfish" style="display:none;background-color:#5a89cc; height: 200px; text-align:center;border-top: 1px solid #000;">

        <div class="text-center">
            <img src="{{ url('banners/top.png') }}" />
        </div>

        <p style="color: #FFFFFF;">This is a simple Catfish Ad. The content can be any HTML code including text, image, form. Change it to your ad content.</p>

        <a href="#" id="catfish-close" style="position:absolute;top:5px;right:15px;color:#FFF;">Close</a>

    </div>
@endsection

@section('right')
    @include('post._vertical_list', ['lists' => $rights_lists])
    <div class="text-center">
    </div>
{{--    @include('post._statistics')--}}
@endsection

@section('script')
    {{--<script type="text/javascript" src="//g.adspeed.net/ad.php?do=js&aid=242760&oid=181&wd=1&ht=1&target=_blank"></script>--}}
    {{--<script type="text/javascript">--}}
        {{--$( document ).ready(function() {--}}
            {{--$('#catfish').catfish({closeLink:'#catfish-close', height:'200'});--}}
        {{--});--}}
    {{--</script>--}}
@endsection
