<!-- Subscribe -->
<section class="alternate section-xs">
    <div class="container-fluid">

        <div class="row text-center">
            <div class="col-lg-4 col-sm-8 offset-lg-4 offset-md-2 offset-sm-2">

                <p class="fs-12 text-muted m-0">Please note: we never sell your email address!</p>
                <h5 class="letter-spacing-1">SUBSCRIBE TO SMARTY NEWSLETTER</h5>
                <div class="input-group">
                    <input id="newsletter_subscribe_email" type="text" class="form-control" placeholder="Type your email...">
								<span class="input-group-btn">
									<button id="newsletter_subscribe_btn" class="btn btn-primary" type="button">SUBSCRIBE</button>
								</span>
                </div><!-- /input-group -->

            </div>

        </div>

    </div>
</section>
<!-- /Subscribe -->
