<nav class="nav d-flex justify-content-between">
    @foreach(\App\Repositories\CategoryRepository::getCategoriesForTopNavigation() as $category)
    <li class="nav-item">
        <a class="p-2 nav-link" href="{{ url('section/'.$category->code) }}">{{ $category->title }}</a>
    </li>
    @endforeach
</nav>
