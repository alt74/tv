<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Новости Энергодара</title>
		<meta name="description" content="" />
		<meta name="Author" content="z@alt.pp.ua" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- Bootstrap core CSS -->
		<link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- THEME CSS -->
		<link href="/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="/css/color_scheme/darkblue.css" rel="stylesheet" type="text/css" id="color_scheme" />

		<link href="/css/app.css" rel="stylesheet" type="text/css" />
		<style>
			.blog-header {
				line-height: 1;
				border-bottom: 1px solid #e5e5e5;
			}

			.blog-header-logo {
				font-family: "Playfair Display", Georgia, "Times New Roman", serif;
				font-size: 2.25rem;
			}

			.blog-header-logo:hover {
				text-decoration: none;
			}
		</style>
	</head>

	<!--
		AVAILABLE BODY CLASSES:
		
		smoothscroll 			= create a browser smooth scroll
		enable-animation		= enable WOW animations

		bg-grey					= grey background
		grain-grey				= grey grain background
		grain-blue				= blue grain background
		grain-green				= green grain background
		grain-blue				= blue grain background
		grain-orange			= orange grain background
		grain-yellow			= yellow grain background
		
		boxed 					= boxed layout
		pattern1 ... patern11	= pattern background
		menu-vertical-hide		= hidden, open on click
		
		BACKGROUND IMAGE [together with .boxed class]
		data-background="assets/images/_smarty/boxed_background/1.jpg"
	-->
	<body class=" -enable-animation">

		<!-- wrapper -->
		<div id="wrapper">

			<!--
				AVAILABLE HEADER CLASSES

				Default nav height: 96px
				.header-md 		= 70px nav height
				.header-sm 		= 60px nav height

				.b-0 		= remove bottom border (only with transparent use)
				.transparent	= transparent header
				.translucent	= translucent header
				.sticky			= sticky header
				.static			= static header
				.dark			= dark header
				.bottom			= header on bottom
				
				shadow-before-1 = shadow 1 header top
				shadow-after-1 	= shadow 1 header bottom
				shadow-before-2 = shadow 2 header top
				shadow-after-2 	= shadow 2 header bottom
				shadow-before-3 = shadow 3 header top
				shadow-after-3 	= shadow 3 header bottom

				.clearfix		= required for mobile menu, do not remove!

				Example Usage:  class="clearfix sticky header-sm transparent b-0"
			-->
			<div class="container">
				<header class="blog-header pb-3">
					<div class="row flex-nowrap justify-content-between align-items-center logo">
						<div class="col-4 pt-1">
							<a class="blog-header-logo text-dark" href="{{ url('/') }}"><img src="{{ url('images/logo.jpg') }}" alt="" height="90" /></a>
						</div>
						<div class="col-4 text-center">

						</div>
						<div class="col-4 d-flex justify-content-end align-items-center">
							{{--<a class="text-muted" href="#">--}}
								{{--<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mx-3"><circle cx="10.5" cy="10.5" r="7.5"></circle><line x1="21" y1="21" x2="15.8" y2="15.8"></line></svg>--}}
							{{--</a>--}}
							@include('layouts.parts.socials')

						</div>
					</div>
				</header>
				<div class="nav-scroller py-1 mb-2">
					@include('layouts.parts.top_navigation')
				</div>
			</div>

			<!-- -->
			<section>
				<div class="container">
					<div class="row">
						<!-- LEFT -->
						<div class="col-sm-9">
						@yield('content')
						</div>
						<!-- /LEFT -->

						<!-- RIGHT -->
						<div class="col-sm-3">
							@yield('right')
						</div>
						<!-- /RIGHT -->
					</div>
				</div>
			</section>
			<!-- / -->

			@include('layouts.parts.subscribe')
			@include('layouts.parts.footer')
		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


{{--		@include('layouts.parts.preloader')--}}

		<!-- Bootstrap core JavaScript
        ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"
				integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
				crossorigin="anonymous"></script>

		<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

		<!-- Icons https://feathericons.com/ -->
		<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
		<script>
			feather.replace()
		</script>

		@yield('script')
	</body>
</html>