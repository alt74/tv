<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin page</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/admin.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('admin') }}">TV Chanel administation page</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ url('logout') }}">Sign out</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Content</span>
                </h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.post.')) active @endif" href="{{ url('admin/post') }}">
                            <span data-feather="file-text"></span>
                            Posts
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.image.')) active @endif" href="{{ url('admin/image') }}">
                            <span data-feather="image"></span>
                            Images
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.video.')) active @endif" href="{{ url('admin/video') }}">
                            <span data-feather="video"></span>
                            Videos
                        </a>
                    </li>
                </ul>

                <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Dictionaries</span>
                </h5>
                <ul class="nav flex-column mb-2">
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.category.')) active @endif" href="{{ url('admin/category') }}">
                            <span data-feather="chevrons-right"></span>
                            Categories
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.author.')) active @endif" href="{{ url('admin/author') }}">
                            <span data-feather="chevrons-right"></span>
                            Authors
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.source.')) active @endif" href="{{ url('admin/source') }}">
                            <span data-feather="chevrons-right"></span>
                            Sources
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.tag.')) active @endif" href="{{ url('admin/tag') }}">
                            <span data-feather="tag"></span>
                            Tags
                        </a>
                    </li>
                </ul>

                <hr>

                <ul class="nav flex-column mb-2">
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), 'change_password')) active @endif" href="{{ url('admin/change_password') }}">
                            <span data-feather="chevrons-right"></span>
                            Change password
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(strpos(request()->route()->getName(), '.setting.')) active @endif" href="{{ url('admin/setting') }}">
                            <span data-feather="settings"></span>
                            Settings
                        </a>
                    </li>
                </ul>

            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            @yield('content')
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"
        integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
        crossorigin="anonymous"></script>

<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Icons https://feathericons.com/ -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

@yield('script')

</body>
</html>
