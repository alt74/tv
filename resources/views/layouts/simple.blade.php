<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/css/simple.css" rel="stylesheet">

</head>

<body>
<div class="container">
    <div class="row">
        <div class="col">
            @yield('content')
        </div>
    </div>
</div>


</body>
</html>