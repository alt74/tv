@extends('layouts.admin')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 ">
        <h1 class="h2">{{ $titles['caption'] }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button type="button" class="btn btn-primary btn-lg" onclick="showAddForm(); return false;">{{ $titles['add_button'] }}</button>
        </div>
    </div>


    @if($errors->count())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    @if(session()->has('status'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('status') }}
        </div>
    @endif


    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            @if (in_array('code', $edit_fields)) <th scope="col">Code</th> @endif
            <th scope="col">Title</th>
            @if (in_array('status', $edit_fields)) <th scope="col">Status</th> @endif
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
        <tr id="row-{{ $item->id }}">
            <th scope="row">{{ $item->id }}</th>
            @if (in_array('code', $edit_fields)) <td
                    class="code"
                    data-status="{{ $item->status }}"
                    data-priority="{{ $item->priority }}"
                    data-options="{{ $item->options }}"
            >{{ $item->code }}</td> @endif
            <td class="title">{{ $item->title }}</td>
            @if (in_array('status', $edit_fields)) <td class="status">{{ $item->status }}</td> @endif
            <td>
                <div class="text-right">
                    <a href="" onclick="showEditForm({{ $item->id }}); return false;" class="mr-3"><span data-feather="edit"></span></a>
                    <a href="" onclick="deleteOne({{ $item->id }}); return false;" class="mr-3"><span data-feather="delete"></span></a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @if (in_array('description', $edit_fields))
    @foreach($list as $item)
        <template id="description-{{ $item->id }}">
            {{ $item->description }}
        </template>
    @endforeach
    @endif

    @include('pagination.default', ['paginator' => $list])

    <div class="modal" id="form-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url($url) }}">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}

                        @if (in_array('code', $edit_fields))
                        <div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
                            <label class="control-label" for="title">Code</label>
                            <input type="text" id="code" class="form-control" id="code" name="code" placeholder="Code" value="{{ old('code', (isset($info->code) ? $info->code : '')) }}">
                        </div>
                        @endif

                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                            <label class="control-label" for="title">Title</label>
                            <input type="text" id="title" class="form-control" name="title" placeholder="Title" value="{{ old('title', (isset($info->title) ? $info->title : '')) }}">
                        </div>

                        @if (in_array('description', $edit_fields))
                        <div class="form-group">
                            <label for="in-description">Description</label>
                            <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '  is-valid' }}" id="in-description" rows="7" name="description" aria-describedby="descriptionHelp">{{ old('description', (isset($info->description) ? $info->description : '')) }}</textarea>
                            @if ($errors->has('description'))
                                <span id="descriptionHelp" class="invalid-feedback">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                        @endif

                        @if (in_array('status', $edit_fields))
                        <div class="form-group">
                            <label for="in-status">Status</label>
                            <select class="form-control" id="in-status" name="status">
                                @foreach(\App\Repositories\DictionRepository::categoryStatuses() as $id => $title)
                                    <option value="{{ $id }}" @if(!empty($info->status) && $info->status==$id) selected @endif>{{ $title }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif

                        @if (in_array('options', $edit_fields))
                        <div class="form-group">
                            <label for="in-options">Options</label>
                            <input type="hidden" name="options_array[0]" value="0" />
                            @foreach(\App\Repositories\DictionRepository::categoryOptions() as $id => $title)
                                <div class="form-check">
                                    <input class="form-check-input" name="options_array[{{$id}}]" type="checkbox" value="{{ $id }}" id="option-{{$id}}"
                                           @if(!empty($info->options) && ($info->options & $id)) checked @endif
                                    >
                                    <label class="form-check-label" for="option-{{$id}}">{{ $title }}</label>
                                </div>
                            @endforeach
                        </div>
                        @endif

                        @if (in_array('priority', $edit_fields))
                        <div class="form-group">
                            <label for="in-priority">Priority</label>
                            <select class="form-control" id="in-priority" name="prioryty">
                                @foreach(\App\Repositories\DictionRepository::categoryPriorities() as $id => $title)
                                    <option value="{{ $id }}" @if(!empty($info->priority) && $info->priority==$id) selected @endif>{{ $title }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="saveChanges();">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('script')
    <script type="text/javascript" src="/vendor/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.6/combined/js/gijgo.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.6/combined/css/gijgo.min.css" rel="stylesheet">

    <script>
        var form_url = '{{ url($url) }}'
          , form_id  = 0;

        function showAddForm() {
            form_id = 0;
            $('#title').val('');
            $('#form-modal .modal-title').html('{{ $titles['add_modal'] }}');
            $('#form-modal').modal('show');
        }

        function showEditForm(id) {
            form_id = id;
            $('#form-modal .modal-title').html('{{ $titles['edit_modal'] }}');
            $('#title').val($('#row-'+id+' .title').html());
            $('#code').val($('#row-'+id+' .code').html());
            $('#in-status').val($('#row-'+id+' .code').data('status'));
            $('.form-check-input').each(function() {
                if ($(this).val() & $('#row-'+id+' .code').data('options')) {
                    $(this).prop('checked', true);
                }
            });
            $('#priority').val($('#row-'+id+' .code').data('priority'));
            $("#in-description")
                    .editor()
                    .content($('#description-'+id).html());
            $('#form-modal').modal('show');
        }

        function saveChanges() {
            if (form_id == 0) {
                $('#form-modal form input[name=_method]').val("POST");
                $('#form-modal form').prop("action", form_url);
            } else {
                $('#form-modal form input[name=_method]').val("PUT");
                $('#form-modal form').prop("action", form_url + '/' + form_id);
            }
            $('#form-modal form').submit();
        }

        function deleteOne(id) {
            bootbox.confirm('Are you realy want to delete the one?', function(v) {
                if (v) {
                    $('#form-modal form input[name=_method]').val("DELETE");
                    $('#form-modal form')
                            .prop("action", form_url + '/' + id)
                            .submit();
                }
            });
        }

        $(document).ready(function () {
            $("#in-description").editor({
                height: 400,
                uiLibrary: 'bootstrap4'
            });
        });

    </script>

@endsection
