@extends('layouts.admin')

@section('content')
    <form role="form" method="POST" action="{{ url('/admin/post') }}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="POST">
        {{ csrf_field() }}

        <h1 class="h3 mb-3 font-weight-normal">Add Post</h1>

        @include('general.messages')

        @include('admin.posts.form')

    </form>

@endsection

@section('script')
    @include('admin.posts.form-script')
@endsection
