<div class="form-group">
    <label for="in-title">Title</label>
    <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '  is-valid' }}" id="in-title" placeholder="Title" name="title" aria-describedby="titleHelp" value="{{ old('title', (isset($info->title) ? $info->title : '')) }}">
    @if ($errors->has('title'))
        <span id="titleHelp" class="invalid-feedback">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>


<div class="form-group">
    <label for="in-description">Brief</label>
    <textarea class="form-control{{ $errors->has('brief') ? ' is-invalid' : '  is-valid' }}" id="in-brief" rows="3" name="brief" aria-describedby="briefHelp">{{ old('brief', (isset($info->brief) ? $info->brief : '')) }}</textarea>
    @if ($errors->has('brief'))
        <span id="briefHelp" class="invalid-feedback">
            <strong>{{ $errors->first('brief') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="in-description">Content</label>
    <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : '  is-valid' }}" id="in-content" rows="7" name="content" aria-describedby="contentHelp">{{ old('content', (isset($info->content) ? $info->content : '')) }}</textarea>
    @if ($errors->has('content'))
        <span id="contentHelp" class="invalid-feedback">
            <strong>{{ $errors->first('content') }}</strong>
        </span>
    @endif
</div>

<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-4">
        @include('fields.datetime', ['fieldname_date' => 'date_d', 'fieldname_time' => 'date_t', 'info' => (isset($info)?$info:null), 'fieldtitle' => 'Date'])
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
        @include('fields.datetime', ['fieldname_date' => 'date_begin_d', 'fieldname_time' => 'date_begin_t', 'info' => (isset($info)?$info:null), 'fieldtitle' => 'Date Begin'])
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
        @include('fields.datetime', ['fieldname_date' => 'date_end_d', 'fieldname_time' => 'date_end_t', 'info' => (isset($info)?$info:null), 'fieldtitle' => 'Date End'])
    </div>
</div>

<div class="form-group">
    <label for="in-category_id">Category</label>
    <select class="form-control" id="in-category_id" name="category_id">
        <option value="0">Not Specified</option>
        @foreach($categories as $item)
            <option value="{{ $item->id }}" @if($info->category_id==$item->id) selected @endif>{{ $item->title }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="in-author_id">Author</label>
    <select class="form-control" id="in-author_id" name="author_id">
        <option value="0">Not Specified</option>
        @foreach($authors as $item)
            <option value="{{ $item->id }}" @if($info->author_id==$item->id) selected @endif>{{ $item->title }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="in-source_id">Source</label>
    <select class="form-control" id="in-source_id" name="source_id">
        <option value="0">Not Specified</option>
        @foreach($sources as $item)
            <option value="{{ $item->id }}" @if($info->source_id==$item->id) selected @endif>{{ $item->title }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="in-priority">Priority</label>
    <select class="form-control" id="in-priority" name="priority">
        @foreach(\App\Repositories\PostBaseRepository::priorities() as $id => $title)
            <option value="{{ $id }}" @if($info->priority==$id) selected @endif>{{ $title }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="in-status">Status</label>
    <select class="form-control" id="in-status" name="status">
        @foreach(\App\Repositories\PostBaseRepository::statuses() as $id => $title)
            <option value="{{ $id }}" @if($info->status==$id) selected @endif>{{ $title }}</option>
        @endforeach
    </select>
</div>



<div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
    <label class="control-label" for="tags">Tags</label>
    <input type="text" class="form-control" id="tags" name="tags"
           data-tokens="{{ old('tags', (isset($tags) ? $tags : '')) }}"
           value="{{ old('tags', (isset($tags) ? $tags : '')) }}"
    >
</div>


{{-- Images --}}
<div>
    <div class="clearfix" id="post-image">
        @foreach($info->images as $image)
        @include('admin.posts.image-item', ['item' => $image])
        @endforeach
    </div>
    <div class="mt-3">
        <a href="" type="button" class="btn btn-default" onclick="showImageModal(); return false;">Add Image</a>
    </div>

</div>

{{-- Videos --}}
<div class="mt-4">
    <div class="clearfix" id="post-video">
        @foreach($info->videos as $video)
            @include('admin.posts.video-item', ['item' => $video])
        @endforeach
    </div>
    <div class="mt-3">
        <a href="" type="button" class="btn btn-default" onclick="showVideoModal(); return false;">Add Video</a>
    </div>
</div>


<div class="form-group mt-4">
    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
</div>

<!-- Image Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageModalLabel">Add Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <input type="text" class="form-control mx-3" name="search_image" id="search_image" placeholder="Search" value="">
                    <input type="button" class="btn mr-3" value="Search" id="btn_search_image">
                </div>
                <div id="media-image">

                </div>
                <div>
                    <div id="loader-image" class="mx-auto" style=" width: 70px;"><img src="{{ url('images/ajax-loader.gif') }}"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_select_image" onclick="selectMedia('image')">Select Image</button>
            </div>
        </div>
    </div>
</div>

<!-- Video Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="videoModalLabel">Add Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <input type="text" class="form-control mx-3" name="search_video" id="search_video" placeholder="Search" value="">
                    <input type="button" class="btn mr-3" value="Search" id="btn_search_video">
                </div>
                <div id="media-video">

                </div>
                <div id="loader-video" class="mx-auto" style=" width: 70px;"><img src="{{ url('images/ajax-loader.gif') }}"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_select_video" onclick="selectMedia('video')">Select Video</button>
            </div>
        </div>
    </div>
</div>