<div class="card pull-left mr-2" style="width: 18rem;" id="image-{{ $item->id }}">
    <div class="card-body">
        <h5 class="card-title">{{ $item->title }}</h5>
        <img class="card-img" src="{{ url('/image/'.$item->id.'/thumbnail') }}" alt="">
        <a href="#" class="card-link" onclick="$(this).parent().parent().remove();"><span data-feather="delete"></span></a>
        <input type="hidden" name="images[]" value="{{ $item->id }}" />
    </div>
</div>
