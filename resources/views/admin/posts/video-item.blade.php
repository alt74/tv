<div class="card pull-left mr-2" style="width: 18rem;" id="video-{{ $item->id }}">
    <div class="card-body">
        <h5 class="card-title">{{ $item->title }}</h5>
        <img class="card-img" src="{{ $item->thumbnail }}" alt="">
        <a href="#" class="card-link" onclick="$(this).parent().parent().remove();"><span data-feather="delete"></a>
        <input type="hidden" name="videos[]" value="{{ $item->id }}" />
    </div>
</div>
