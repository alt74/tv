<script type="text/javascript" src="/vendor/moment/moment.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/gijgo@1.8.1/combined/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.8.1/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />

{{--https://github.com/sliptree/bootstrap-tokenfield--}}
{{--https://stackoverflow.com/questions/43426304/how-should-i-use-bootstrap-3-tokenfield-with-autocomplete-and-typeahead--}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.css">

{{--https://github.com/twitter/typeahead.js--}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.1/typeahead.bundle.js"></script>

<script type="text/javascript">
    $(function () {
//            http://gijgo.com/timepicker
//            http://gijgo.com/datepicker
        $('#date_d').datepicker({
            format: 'dd.mm.yyyy',
            weekStartDay: 1,
            calendarWeeks: true
        });
        $('#date_begin_d').datepicker({
            format: 'dd.mm.yyyy',
            weekStartDay: 1,
            calendarWeeks: true
        });
        $('#date_end_d').datepicker({
            format: 'dd.mm.yyyy',
            weekStartDay: 1,
            calendarWeeks: true
        });
        $('#date_t').timepicker();
        $('#date_begin_t').timepicker();
        $('#date_end_t').timepicker();

        $('#btn_search_image').on('click', function() {
            loadMedia($('#search_image').val(), 'image', 1);
        });
        $('#btn_search_video').on('click', function() {
            loadMedia($('#search_video').val(), 'video', 1);
        });


//                    https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md
        var engine = new Bloodhound({
            remote: {
                url: '{{ url('tags/typeahead') }}?query=%QUERY'
            },
            datumTokenizer: function(d) {
                return Bloodhound.tokenizers.whitespace(d.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
        engine.initialize();
        $('input#tags').tokenfield({
            typeahead: [null, { source: engine.ttAdapter() }]
        });

    });

    function showImageModal() {
        $('#imageModal').modal('show');
        loadMedia($('#search_image').val(), 'image', 1);
    }

    function showVideoModal() {
        $('#videoModal').modal('show');
        loadMedia($('#search_video').val(), 'video', 1);
    }

    function loadMedia(search, type, page) {
        $('#loader-'+type).show();
        $('#media-'+type).hide();
        $.get('{{ url('/admin/post/media') }}/'+type, {
                'search': search,
                'page': page
            },
            function(html){
                $('#media-'+type).html(html).show();
                $('.aj .page-link').off('click');
                $('.aj .page-link').on('click', function(e) {
                    e.stopPropagation();
                    var click_page = $(this).attr('href').substr(6);
                    loadMedia(search, type, click_page);
                    return false;
                });
            }
        ).always(function() {
          $('#loader-'+type).hide();
        });
    }

    function selectMedia(type) {
        var id = $('input[name='+type+']:checked').val();
        $('#'+type+'Modal').modal('hide');
        $.get('{{ url('/admin/post/media/selected') }}/'+type+'/'+id,
            function(html){
                $('#post-'+type).append(html);
            }
            ).always(function() {
                $('#loader-'+type).hide();
            });
    }

</script>
