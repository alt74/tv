<div class="mb-3">
    @foreach ($list->chunk(3) as $chunk)
        <div class="card-deck"  style="margin-top: 20px;">
            @foreach ($chunk as $i => $item)
                <div class="card" >

                    <div class="card-body">
                        <img class="card-img" src="{{ url('/image/'.$item->id.'/thumbnail') }}" alt="">
                        <div>
                            {{ $item->title }}
                            <small class="text-muted">
                                @foreach($item->tags as $tag)
                                    <span data-feather="tag"></span>
                                    {{ $tag->title }}
                                @endforeach
                            </small>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="radio" name="image" value="{{ $item->id }}" @if($i == 0) checked @endif/>
                    </div>
                </div>
            @endforeach
            @for($i=0; $i< (3 - $chunk->count()); $i++)
                <div class="card" style="width: 18rem;"></div>
            @endfor
        </div>
    @endforeach
</div>

<div class="aj">
    @include('pagination.default', ['paginator' => $list])
</div>
