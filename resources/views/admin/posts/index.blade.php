@extends('layouts.admin')

@section('content')

<form>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Posts</h1>


        <input type="text" class="form-control mx-3" name="search" placeholder="Search Post" value="{{ $search }}">
        <input type="submit" class="btn mr-3" value="Search">
        <input type="reset" class="btn mr-3" value="Reset" onclick="location='{{ url('/admin/post') }}'">

        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{ url('/admin/post/create') }}" type="button" class="btn btn-primary btn-lg">Add Post</a>
        </div>


    </div>
</form>

    @include('general.messages')
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Date</th>
        <th scope="col">Category</th>
        <th scope="col">Title</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $item)
        <tr id="row-{{ $item->id }}">
            <th scope="row">{{ $item->id }}</th>
            <td class="title">{{ $item->date_d }} {{ $item->date_t }}</td>
            <td class="title">{{ $item->category ? $item->category->title : '' }}</td>
            <td class="title">{{ $item->title }}</td>
            <td class="text-right">
                <a href="{{ url('/admin/post/'.$item->id.'/edit') }}" class="mr-3"><span data-feather="edit"></span></a>
                <a href="" onclick="deletePost({{ $item->id }}); return false;" class="mr-3"><span data-feather="delete"></span></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@include('pagination.default', ['paginator' => $list])
<form method="post" id="delete-post">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>

@endsection

@section('script')
    <script type="text/javascript" src="/vendor/bootbox/bootbox.js"></script>

    <script>
        function deletePost(id) {
            bootbox.confirm('Are you realy want to delete the one?', function(v) {
                if (v) {
                    $('#delete-post')
                            .prop('action', '{{ url('/admin/post') }}/'+id)
                            .submit();
                }
            });

        }
    </script>
@endsection