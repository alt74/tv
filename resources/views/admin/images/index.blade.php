@extends('layouts.admin')

@section('content')
    <form>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Images</h1>


        <input type="text" class="form-control mx-3" name="search" value="{{ $search }}" placeholder="Search Image">
        <input type="submit" class="btn mr-3" value="Search">
        <input type="reset" class="btn mr-3" value="Reset" onclick="location='{{ url('/admin/image') }}'">

        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{ url('/admin/image/create') }}" type="button" class="btn btn-primary btn-lg">Add Image</a>
        </div>


    </div>
    </form>

    @include('general.messages')

    <div>
        @foreach ($list->chunk(6) as $chunk)
            <div class="card-deck"  style="margin-top: 20px;">
                @foreach ($chunk as $item)
                    <div class="card" >

                        <div class="card-body">
                            <img class="card-img" src="{{ url('/image/'.$item->id.'/thumbnail') }}" alt="">
                            <div>{{ $item->title }}</div>
                            <small class="text-muted">
                            @foreach($item->tags as $tag)
                            <span data-feather="tag"></span>
                            {{ $tag->title }}
                            @endforeach
                            </small>
                        </div>
                        <div class="card-footer text-right">
                            <a href="{{ url('/admin/image/'.$item->id.'/edit') }}" class="card-link"><span data-feather="edit"></span></a>
                            <a href="" onclick="deleteImage({{ $item->id }}); return false;" class="card-link"><span data-feather="delete"></span></a>
                        </div>
                    </div>
                @endforeach
                @for($i=0; $i< (6 - $chunk->count()); $i++)
                    <div class="card" style="width: 18rem;"></div>
                @endfor
            </div>
        @endforeach
    </div>

    @include('pagination.default', ['paginator' => $list])

    <form method="post" id="delete-image">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@endsection

@section('script')
    <script type="text/javascript" src="/vendor/bootbox/bootbox.js"></script>

    <script>
    function deleteImage(id) {
        bootbox.confirm('Are you realy want to delete the one?', function(v) {
            if (v) {
                $('#delete-image')
                        .prop('action', '{{ url('/admin/image') }}/'+id)
                        .submit();
            }
        });

    }
    </script>
@endsection

