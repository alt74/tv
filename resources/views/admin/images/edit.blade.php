@extends('layouts.admin')

@section('content')
    <form role="form" method="POST" action="{{ url('/admin/image/'.$info->id) }}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}

        <h1 class="h3 mb-3 font-weight-normal">Edit Image</h1>

        @include('general.messages')

        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="card-deck"  style="margin-top: 20px;">
                        <div class="card">
                            <img class="card-img-top" src="{{ url('/image/'.$info->id) }}" alt="{{ $info->title }}">
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    @include('admin.images.form')
                </div>
            </div>
        </div>

    </form>

@endsection

@section('script')
    {{--https://github.com/sliptree/bootstrap-tokenfield--}}
    {{--https://stackoverflow.com/questions/43426304/how-should-i-use-bootstrap-3-tokenfield-with-autocomplete-and-typeahead--}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.css">

    {{--https://github.com/twitter/typeahead.js--}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.1/typeahead.bundle.js"></script>

    <script type="text/javascript">
        $(function () {
//                    https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md
            var engine = new Bloodhound({
                remote: {
                    url: '{{ url('tags/typeahead') }}?query=%QUERY'
                },
                datumTokenizer: function(d) {
                    return Bloodhound.tokenizers.whitespace(d.value);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });
            engine.initialize();
            $('input#tags').tokenfield({
                typeahead: [null, { source: engine.ttAdapter() }]
            });

        });

    </script>

@endsection
