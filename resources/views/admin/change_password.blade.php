@extends('layouts.admin')

@section('content')

    <form role="form" method="POST" action="{{ url('/admin/change_password') }}">
        {{ csrf_field() }}

        <h1 class="h3 mb-3 font-weight-normal">Change Password</h1>

        @if($errors->count())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="form-group">
            <label for="in-password">Old Password</label>
            <input type="password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '  is-valid' }}" id="in-old-password" placeholder="Old Password" name="old_password" aria-describedby="oldPasswordHelp">
            @if ($errors->has('old_password'))
                <span id="oldPasswordHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('old_password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-password">Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-password" placeholder="Password" name="password" aria-describedby="passwordHelp">
            @if ($errors->has('password'))
                <span id="passwordHelp" class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="in-confirm-password">Confirm Password</label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '  is-valid' }}" id="in-confirm-password" placeholder="Confirm Password" name="password_confirmation" aria-describedby="passwordConfirmationHelp">
            @if ($errors->has('password_confirmation'))
                <span id="password_confirmation" class="invalid-feedback">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Change Password</button>
        </div>

    </form>

@endsection
