@extends('layouts.admin')

@section('content')

<form>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Videos</h1>

        <input type="text" class="form-control mx-3" name="search" placeholder="Search Video" value="{{ $search }}">
        <input type="submit" class="btn mr-3" value="Search">
        <input type="reset" class="btn mr-3" value="Reset" onclick="location='{{ url('/admin/video') }}'">

        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{ url('/admin/video/create') }}" type="button" class="btn btn-primary btn-lg">Add Video</a>
        </div>

    </div>
</form>

    @include('general.messages')

    <div>
        <div>
            @foreach ($list->chunk(6) as $chunk)
                <div class="card-deck"  style="margin-top: 20px;">
                    @foreach ($chunk as $item)
                        <div class="card" >

                            <div class="card-body">
                                <a href="{{ url('/admin/video/'.$item->id) }}"><img class="card-img" src="{{ $item->thumbnail }}" alt=""></a>
                                <div>{{ $item->title }}</div>
                                <small class="text-muted">
                                    @foreach($item->tags as $tag)
                                        <span data-feather="tag"></span>
                                        {{ $tag->title }}
                                    @endforeach
                                </small>
                            </div>
                            <div class="card-footer text-right">
                                <a href="{{ url('/admin/video/'.$item->id.'/edit') }}" class="card-link"><span data-feather="edit"></a>
                                <a href="" onclick="deleteVideo({{ $item->id }}); return false;" class="card-link"><span data-feather="delete"></a>
                            </div>
                        </div>
                    @endforeach
                    @for($i=0; $i< (6 - $chunk->count()); $i++)
                        <div class="card" style="width: 18rem;"></div>
                    @endfor
                </div>
            @endforeach
        </div>

        @include('pagination.default', ['paginator' => $list])

        <form method="post" id="delete-video">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="/vendor/bootbox/bootbox.js"></script>

    <script>
        function deleteVideo(id) {
            bootbox.confirm('Are you realy want to delete the one?', function(v) {
                if (v) {
                    $('#delete-video')
                            .prop('action', '{{ url('/admin/video') }}/'+id)
                            .submit();
                }
            });

        }
    </script>
@endsection