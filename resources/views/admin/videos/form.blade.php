<div class="form-group">
    <label for="in-title">Video URL</label>
    <input type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '  is-valid' }}" id="in-url" placeholder="Video URL" name="url" aria-describedby="urlHelp" value="{{ old('url', (isset($info->url) ? $info->url : '')) }}">
    @if ($errors->has('url'))
        <span id="urlHelp" class="invalid-feedback">
            <strong>{{ $errors->first('url') }}</strong>
        </span>
    @endif
</div>


<div class="form-group">
    <label for="in-title">Title</label>
    <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '  is-valid' }}" id="in-title" placeholder="Title" name="title" aria-describedby="titleHelp" value="{{ old('title', (isset($info->title) ? $info->title : '')) }}">
    @if ($errors->has('title'))
        <span id="titleHelp" class="invalid-feedback">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>


<div class="form-group">
    <label for="in-description">Description</label>
    <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '  is-valid' }}" id="in-description" rows="3" name="description" aria-describedby="descriptionHelp">{{ old('description', (isset($info->description) ? $info->description : '')) }}</textarea>
    @if ($errors->has('description'))
        <span id="descriptionHelp" class="invalid-feedback">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
    <label class="control-label" for="tags">Tags</label>
    <input type="text" class="form-control" id="tags" name="tags"
           data-tokens="{{ old('tags', (isset($tags) ? $tags : '')) }}"
           value="{{ old('tags', (isset($tags) ? $tags : '')) }}"
            >
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
