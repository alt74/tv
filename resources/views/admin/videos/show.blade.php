@extends('layouts.admin')

@section('content')
    <h1 class="h3 mb-3 font-weight-normal">{{ $info->title }}</h1>

    @include('general.messages')

    <div class="card" >

        <div class="card-body">
            <div class="embed-responsive embed-responsive-16by9">
                {!! $info->html !!}
            </div>

            <p class="card-text mt-4">{{ $info->description }}</p>
            <small class="text-muted">
                @foreach($info->tags as $tag)
                    <span data-feather="tag"></span>
                    {{ $tag->title }}
                @endforeach
            </small>
        </div>
        <div class="card-footer">
            <div><small>[{{ $info->created_at->format('d.m.y H:i:s') }}]</small></div>
        </div>
    </div>

@endsection

