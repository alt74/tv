@extends('layouts.admin')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 ">
        <h1 class="h2">{{ $titles['caption'] }}</h1>
    </div>


    @if($errors->count())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    @if(session()->has('status'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('status') }}
        </div>
    @endif


    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Setting</th>
            <th scope="col">Value</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
        <tr id="row-{{ $item->id }}">
            <th scope="row">{{ $item->key }}</th>
            <td >{{ $item->description }}</td>
            <td class="title">{{ $item->value }}</td>
            <td>
                <div class="col-md-3 float-md-right">
                    <a href="" onclick="showEditForm({{ $item->id }}); return false;">Edit</a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @include('pagination.default', ['paginator' => $list])

    <div class="modal" id="form-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url($url) }}">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                            <label class="control-label" for="title"></label>
                            <input type="text" id="title" class="form-control" id="title" name="value" placeholder="Value" value="{{ old('value') }}">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="saveChanges();">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('script')
    <script type="text/javascript" src="/vendor/bootbox/bootbox.js"></script>

    <script>
        var form_url = '{{ url($url) }}'
          , form_id  = 0;

        function showEditForm(id) {
            form_id = id;
            $('#form-modal .modal-title').html('{{ $titles['edit_modal'] }}');
            $('#title').val($('#row-'+id+' .title').html());

            $('#form-modal').modal('show');
        }

        function saveChanges() {
            $('#form-modal form input[name=_method]').val("PUT");
            $('#form-modal form').prop("action", form_url + '/' + form_id);
            $('#form-modal form').submit();
        }

    </script>

@endsection
