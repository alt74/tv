<?php


Route::get('/', 'PostController@index');
Route::get('/section/{code}', 'PostController@section');
Route::get('/search', 'PostController@search');
Route::get('/post/{id}', 'PostController@show');


Route::auth();

Route::group(['middleware' => 'role:admin', 'namespace' => 'Admin'], function () { /*role:admin*/

    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'PostController@index');

        Route::resource('category', 'CategoryController');
        Route::resource('post', 'PostController');
        Route::resource('author', 'AuthorController');
        Route::resource('source', 'SourceController');
        Route::resource('tag', 'TagController');
        Route::resource('image', 'ImageController');
        Route::resource('video', 'VideoController');
        Route::resource('setting', 'SettingController');

        Route::get('change_password', 'UserController@passwordForm')->name('admin.change_password');
        Route::post('change_password', 'UserController@changePassword');

        Route::get('post/media/{type}', 'PostController@getMediaList');
        Route::get('post/media/selected/{type}/{id}', 'PostController@getSelectedMediaItem');
    });

});

Route::get('/image/{id}/{type?}', 'ImageController@show');

Route::get('/tags/typeahead', 'TagController@typeahead');

Route::get('/it', function () {
    $i = App\Image::find(7);
    return response(\Storage::get($i->image))
        ->header('Content-Type', 'image/jpeg');
});