<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Repositories\PostRetrieveRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class PostController extends Controller
{

    public function index(PostRetrieveRepository $repository)
    {
        $list = $repository->getIndexList();
        $online = $repository->getOnline();
        $is_online_on = $repository->isOnlineOn();
        $rights_lists = $repository->getRightsLists();

        return view('index', compact('list', 'online', 'rights_lists', 'is_online_on'));
    }


    public function section($code, PostRetrieveRepository $repository)
    {
        $category = CategoryRepository::getCategoryByCode($code);
        $list = $repository->getCategoriesPostList($category);

        return view('post.section', compact('list', 'category'));
    }

    public function show($id, PostRetrieveRepository $repository)
    {
        $post = $repository->getOnePost($id);

        return view('post.one', compact('post'));
    }

    public function search(Requests\SearchPostRequest $request, PostRetrieveRepository $repository)
    {
        $search = $request->query('search');
        $list = $repository->getSearchList($search);

        return view('post.search', compact('list'));
    }

}
