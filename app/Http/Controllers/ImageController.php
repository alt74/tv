<?php

namespace App\Http\Controllers;

use App\Image;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class ImageController extends Controller
{
    protected $repository;

    public function __construct(ImageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function show($id, $type='')
    {
        return response($this->repository->getContentImageByType($id, $type))
            ->header('Content-Type', 'image/jpeg');

    }

}
