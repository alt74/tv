<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;


trait DictionaryTrait
{

    public function index()
    {
        return view('admin.dictionary', [
            'list'          => $this->model->paginate(10),
            'url'           => $this->url,
            'titles'        => $this->titles,
            'edit_fields'   => $this->edit_fields,
        ]);
    }


    public function store(Requests\DictionRequest $request)
    {
        $this->model->create($request->all());
        return redirect($this->url)->with('status', $this->messages['added']);
    }

    public function update(Requests\DictionRequest $request, $id)
    {
        $this->model = $this->model->findOrFail($id);
        $this->model->update($request->all());

        return redirect($this->url)->with('status', $this->messages['edited']);
    }

    public function destroy($id)
    {
        $this->model = $this->model->findOrFail($id);
        $this->model->delete();
        return redirect($this->url)->with('status', $this->messages['deleted']);

    }

}