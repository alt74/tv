<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    protected $repository = null;

    public function __construct(ImageRepository $repository)
    {
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $search = $request->get('search');
        $search_array = explode(',', $search);
        $list = $this->repository->getPaginatedList($this->repository->prepareListSearch($search_array));

        return view('admin.images.index', compact('list', 'search'));
    }

    public function create()
    {
        return view('admin.images.create');
    }

    public function store(Requests\CreateImageRequest $request)
    {
        $model = $this->repository->create($request);
        return redirect('/admin/image')->with('status', 'Image has been added');
    }

    public function edit($id)
    {
        $info = Image::findOrFail($id);
        $tags = implode(',', $info->tags->pluck('title')->toArray());

        return view('admin.images.edit', compact('info', 'tags'));
    }

    public function update(Requests\UpdateImageRequest $request, $id)
    {
        $info = Image::findOrFail($id);
        $this->repository->update($request, $info);
        return redirect('/admin/image')->with('status', 'Image has been update');
    }

    public function destroy($id)
    {
        $info = Image::findOrFail($id);
        $this->repository->delete($info);
        return redirect('/admin/image')->with('status', 'Image has been delete');
    }
}
