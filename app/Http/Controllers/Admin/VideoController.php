<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\VideoRepository;
use App\Video;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    protected $repository = null;

    public function __construct(VideoRepository $repository)
    {
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $search = $request->get('search');
        $search_array = explode(',', $search);
        $list = $this->repository->getPaginatedList($this->repository->prepareListSearch($search_array));

        return view('admin.videos.index', compact('list', 'search'));
    }

    public function create()
    {
        return view('admin.videos.create');
    }

    public function store(Requests\VideoRequest $request)
    {
        $model = $this->repository->create($request);
        return redirect('/admin/video')->with('status', 'Video has been added');
    }

    public function show($id)
    {
        $info = Video::findOrFail($id);
//        $tags = implode(',', $info->tags->pluck('title')->toArray());

        return view('admin.videos.show', compact('info', 'tags'));
    }

    public function edit($id)
    {
        $info = Video::findOrFail($id);
        $tags = implode(',', $info->tags->pluck('title')->toArray());

        return view('admin.videos.edit', compact('info', 'tags'));
    }

    public function update(Requests\VideoRequest $request, $id)
    {
        $info = Video::findOrFail($id);
        $this->repository->update($request, $info);
        return redirect('/admin/video')->with('status', 'Video has been update');
    }

    public function destroy($id)
    {
        $info = Video::findOrFail($id);
        $this->repository->delete($info);
        return redirect('/admin/video')->with('status', 'Video has been delete');
    }
}
