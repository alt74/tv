<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    protected  $model = null;
    protected $url = '/admin/author';
    protected $titles = [
        'caption'       => 'Authors',
        'add_button'    => 'Add Author',
        'add_modal'     => 'Add Author',
        'edit_modal'    => 'Edit Author',
    ];

    protected $messages = [
        'added'      => 'Author Created!',
        'edited'     => 'Author Updated!',
        'deleted'    => 'Author Deleted!',
    ];

    protected $edit_fields = [];

    public function __construct()
    {
        $this->model = new Author();
    }

    use DictionaryTrait;
}
