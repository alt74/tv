<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    protected  $model = null;
    protected $url = '/admin/tag';
    protected $titles = [
        'caption'       => 'Tags',
        'add_button'    => 'Add Tag',
        'add_modal'     => 'Add Tag',
        'edit_modal'    => 'Edit Tag',
    ];

    protected $messages = [
        'added'      => 'Tag Created!',
        'edited'     => 'Tag Updated!',
        'deleted'    => 'Tag Deleted!',
    ];

    protected $edit_fields = [];

    public function __construct()
    {
        $this->model = new Tag();
    }

    use DictionaryTrait;

}
