<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use App\Category;
use App\Post;
use App\Repositories\ImageRepository;
use App\Repositories\PostBaseRepository;
use App\Repositories\PostModifyRepository;
use App\Repositories\PostRetrieveRepository;
use App\Repositories\VideoRepository;
use App\Source;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    protected $modify_repository = null;
    protected $retrieve_repository = null;

    public function __construct(PostModifyRepository $modify_repository, PostRetrieveRepository $retrieve_repository)
    {
        $this->modify_repository = $modify_repository;
        $this->retrieve_repository = $retrieve_repository;
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $search_array = explode(',', $search);
        $list = $this->retrieve_repository->getPaginatedList($this->retrieve_repository->prepareListSearch($search_array));

        return view('admin.posts.index', compact('list', 'search'));
    }

    public function create()
    {
        return view('admin.posts.create', [
            'info'          => new Post(),
            'categories'    => Category::all(),
            'authors'       => Author::all(),
            'sources'       => Source::all()
        ]);
    }

    public function store(Requests\PostRequest $request)
    {
        $model = $this->modify_repository->create($request);
        return redirect('/admin/post')->with('status', 'Post has been added');
    }

    public function show($id)
    {
        $info = Post::findOrFail($id);

        return view('admin.posts.show', compact('info', 'tags'));
    }

    public function edit($id)
    {
        $info = Post::findOrFail($id);
        $tags = implode(',', $info->tags->pluck('title')->toArray());
        $categories = Category::all();
        $authors = Author::all();
        $sources = Source::all();

        return view('admin.posts.edit', compact('info', 'tags', 'categories', 'authors', 'sources'));
    }

    public function update(Requests\PostRequest $request, $id)
    {
        $info = Post::findOrFail($id);
        $this->modify_repository->update($request, $info);
        return redirect('/admin/post')->with('status', 'Post has been update');
    }

    public function destroy($id)
    {
        $info = Post::findOrFail($id);
        $this->modify_repository->delete($info);
        return redirect('/admin/post')->with('status', 'Post has been delete');
    }

    public function getMediaList($type, Request $request)
    {
        switch($type) {
            case 'video':
                $repository = new VideoRepository();
                break;
            default:
                $repository = new ImageRepository();
        }
        return $repository->getModalList($request->query('search'), $request->query('page'));
    }

    public function getSelectedMediaItem($type, $id)
    {
        switch($type) {
            case 'video':
                $repository = new VideoRepository();
                break;
            default:
                $repository = new ImageRepository();
        }
        return $repository->getSelectedMediaItem($id);
    }

}
