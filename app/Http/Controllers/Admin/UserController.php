<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function passwordForm()
    {
        return view('admin.change_password');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|old_password:' . auth()->user()->password,
            'password' => 'required|confirmed',
        ]);
        $user = auth()->user();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect('/admin/change_password')->with('status', 'Password has been changed!');
    }

}
