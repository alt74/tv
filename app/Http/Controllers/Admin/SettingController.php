<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    protected  $model = null;
    protected $url = '/admin/setting';
    protected $titles = [
        'caption'       => 'Settings',
        'edit_modal'    => 'Edit Setting',
    ];

    protected $messages = [
        'edited'     => 'Source Updated!',
    ];

    public function __construct()
    {
        $this->model = new Setting();
    }

    public function index()
    {
        return view('admin.settings', [
            'list' => $this->model->paginate(20),
            'url' => $this->url,
            'titles' => $this->titles,
        ]);
    }


    public function update(Request $request, $id)
    {
        $this->model = $this->model->findOrFail($id);
        $this->model->update($request->all());

        return redirect('admin/setting')->with('status', $this->messages['edited']);
    }


}
