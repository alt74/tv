<?php

namespace App\Http\Controllers\Admin;

use App\Source;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SourceController extends Controller
{
    protected  $model = null;
    protected $url = '/admin/source';
    protected $titles = [
        'caption'       => 'Sources',
        'add_button'    => 'Add Source',
        'add_modal'     => 'Add Source',
        'edit_modal'    => 'Edit Source',
    ];

    protected $messages = [
        'added'      => 'Source Created!',
        'edited'     => 'Source Updated!',
        'deleted'    => 'Source Deleted!',
    ];

    protected $edit_fields = [];

    public function __construct()
    {
        $this->model = new Source();
    }

    use DictionaryTrait;
}
