<?php

namespace App\Http\Controllers\Admin;

use App\Category;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected  $model = null;
    protected $url = '/admin/category';
    protected $titles = [
        'caption'       => 'Categories',
        'add_button'    => 'Add Category',
        'add_modal'     => 'Add Category',
        'edit_modal'    => 'Edit Category',
    ];

    protected $messages = [
        'added'      => 'Category Created!',
        'edited'     => 'Category Updated!',
        'deleted'    => 'Category Deleted!',
    ];

    protected $edit_fields = ['code', 'description', 'status', 'options'];

    public function __construct()
    {
        $this->model = new Category();
    }

    use DictionaryTrait;

}
