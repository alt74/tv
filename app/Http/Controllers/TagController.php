<?php

namespace App\Http\Controllers;

use App\Repositories\TagRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class TagController extends Controller
{

    public function typeahead(Request $request)
    {
        $repository = new TagRepository();
        return $repository->getTypeaheadSearchList($request->get('query'));
    }

}
