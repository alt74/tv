<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if ($role == 'admin'  && !($request->user() && $request->user()->isAdmin())) {
            if (auth()->guest()) {
                return redirect('login');
            } else {
                 abort(403, 'Unauthorized action.');
            }
        }

        return $next($request);
    }
}
