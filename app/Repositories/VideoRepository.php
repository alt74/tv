<?php

namespace App\Repositories;

use App\Http\Requests\Request;
use App\Video;
use Illuminate\Support\Collection;

class VideoRepository
{

    public function create(Request $request)
    {
        $request->merge(['info' => json_encode($this->getVideoData($request->get('url')))]);
        $model = Video::create($request->all());
        $this->syncTags($model, $request->get('tags'));

        return $model;
    }

    public function update(Request $request, Video $model)
    {
        $request->merge(['info' => json_encode($this->getVideoData($request->get('url')))]);
        $model->update($request->all());
        $this->syncTags($model, $request->get('tags'));

        return $model;
    }

    public function delete(Video $model)
    {
        $model->delete();
    }

    private function syncTags(Video $model, $data)
    {
        $names = explode(',', $data);
        $TagRepository = app()->make(TagRepository::class);
        $TagRepository->insertNewItems($names);
        $ids = $TagRepository->getIdsByNames($names);
        $model->tags()->sync($ids);
    }


    public function getPaginatedList(Collection $filter)
    {
        $builder = Video::query();
        if ($filter->count()) {
            $builder->where(function ($query) use ($filter) {
                foreach($filter->toArray() as $type => $filter_type_list) {
                    switch($type) {
                        case 'tag':
                            $query->orWhereHas('tags', function ($query_tags) use ($filter_type_list) {
                                $query_tags->whereIn('title', $filter_type_list);
                            });
                            break;
                        default:
                            foreach($filter_type_list as $value) {
                                $query->orWhere('title', 'like', '%'.$value.'%');
                                $query->orWhere('description', 'like', '%'.$value.'%');
                            }
                    }
                }
            });
        }
        $builder->with('tags');
        $builder->orderBy('created_at', 'desc');

        return $builder->paginate(12);
    }

    public function prepareListSearch($search_array)
    {
        $result = [];
        foreach($search_array as $search_item) {
            if ($search_item) {
                $result['tag'][] = trim($search_item);
                $result['text'][] = trim($search_item);
            }
        }

        return collect($result);
    }

    public function getVideoData($url)
    {
        $embera = new \Embera\Embera();
        return $embera->getUrlInfo($url);
    }

    public function getModalList($search='', $page=1)
    {
        $list = $this->getPaginatedList($this->prepareListSearch(explode(',', $search)), 6);
        $list->setPath('');
        return view('admin.posts.videos', compact('list'));
    }

    public function getSelectedMediaItem($id)
    {
        $item = Video::find($id);
        return view('admin.posts.video-item', compact('item'));
    }


}