<?php

namespace App\Repositories;

use App\Http\Requests\Request;
use App\Image;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ImageRepository
{

    public function __construct()
    {

    }

    public function create(Request $request)
    {
        $model = Image::create($request->all());

        $image = \Intervention\Image\Facades\Image::make($request->file('image'));
        $image->widen(400, function ($constraint) {
            $constraint->aspectRatio();
        });
        Storage::put($model->image, (string) $image->encode());

        $image->widen(100, function ($constraint) {
            $constraint->aspectRatio();
        });
        Storage::put($model->thumbnail, (string) $image->encode());

        $this->syncTags($model, $request->get('tags'));

        return $model;
    }

    public function update(Request $request, Image $model)
    {
        $model->update($request->all());
        if ($request->hasFile('image')) {
            Storage::delete($model->thumbnail);
            Storage::delete($model->image);

            $image = \Intervention\Image\Facades\Image::make($request->file('image'));
            $image->widen(400, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($model->image, (string) $image->encode());

            $image->widen(100, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($model->thumbnail, (string) $image->encode());
        }

        $this->syncTags($model, $request->get('tags'));

        return $model;
    }

    public function delete(Image $model)
    {
        try {
            Storage::delete($model->thumbnail);
            Storage::delete($model->image);
        } catch(\Exception $e) {

        }
        $model->delete();
    }

    private function syncTags(Image $image, $data)
    {
        $names = explode(',', $data);
        $TagRepository = app()->make(TagRepository::class);
        $TagRepository->insertNewItems($names);
        $ids = $TagRepository->getIdsByNames($names);
        $image->tags()->sync($ids);
    }


    public function getPaginatedList(Collection $filter, $count = 12)
    {
        $builder = Image::query();
        if ($filter->count()) {
            $builder->where(function ($query) use ($filter) {
                foreach($filter->toArray() as $type => $filter_type_list) {
                    switch($type) {
                        case 'tag':
                            $query->orWhereHas('tags', function ($query_tags) use ($filter_type_list) {
                                $query_tags->whereIn('title', $filter_type_list);
                            });
                            break;
                        default:
                            foreach($filter_type_list as $value) {
                                $query->orWhere('title', 'like', '%'.$value.'%');
                                $query->orWhere('description', 'like', '%'.$value.'%');
                            }
                    }
                }
            });
        }
        $builder->with('tags');
        $builder->orderBy('created_at', 'desc');

        return $builder->paginate($count);
    }

    public function prepareListSearch($search_array)
    {
        $result = [];
        foreach($search_array as $search_item) {
            if ($search_item) {
                $result['tag'][] = trim($search_item);
                $result['text'][] = trim($search_item);
            }
        }

        return collect($result);
    }


    public function getFilePath($folder, $id)
    {
        $secret = 'f#d';
        return sprintf('public\%s\%s\%d-%s.jpg',
            $folder,
            floor($id/100),
            $id,
            substr(sha1($secret.$id), 0, 12)
        );
    }

    public function getContentImageByType($id, $type)
    {
        try {
            switch($type) {
                case 'thumbnail':
                    $path = $this->getFilePath('thumbnail', $id);
                    break;
                default:
                    $path = $this->getFilePath('image', $id);
            }
            $image = Storage::get($path);
        } catch (\Exception $e) {
            return file_get_contents(resource_path('assets/300x200.png'));
        }
        return $image;
    }

    public function getModalList($search='', $page=1)
    {
        $list = $this->getPaginatedList($this->prepareListSearch(explode(',', $search)), 6);
        $list->setPath('');
        return view('admin.posts.images', compact('list'));
    }

    public function getSelectedMediaItem($id)
    {
        $item = Image::find($id);
        return view('admin.posts.image-item', compact('item'));
    }

}