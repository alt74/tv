<?php

namespace App\Repositories;

use App\Http\Requests\Request;
use App\Post;
use Illuminate\Support\Collection;

class PostModifyRepository
{
    public function create(Request $request)
    {

        $model = Post::create($this->prepareStoreData($request));
        $this->syncTags($model, $request->get('tags'));
        $model->images()->sync($request->get('images')?:[]);
        $model->videos()->sync($request->get('videos')?:[]);

        return $model;
    }

    public function update(Request $request, Post $model)
    {
        $model->update($this->prepareStoreData($request));
        $this->syncTags($model, $request->get('tags'));
        $model->images()->sync($request->get('images')?:[]);
        $model->videos()->sync($request->get('videos')?:[]);

        return $model;
    }

    private function prepareStoreData(Request $request)
    {
        $dictionary_fields = ['category_id', 'author_id', 'source_id'];
        $data = $request->except($dictionary_fields);
        foreach($dictionary_fields as $field) {
            if($value = $request->get($field)) {
                $data[$field] = $value;
            }
        }

        return $data;
    }

    public function delete(Post $model)
    {
        $model->delete();
    }

    private function syncTags(Post $model, $data)
    {
        $names = explode(',', $data);
        $TagRepository = app()->make(TagRepository::class);
        $TagRepository->insertNewItems($names);
        $ids = $TagRepository->getIdsByNames($names);
        $model->tags()->sync($ids);
    }

}