<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tsivun
 * Date: 11.05.2018
 * Time: 13:45
 */

namespace App\Repositories;


use App\Category;

class CategoryRepository
{
    public static function getCategoriesForTopNavigation()
    {
        $list = Category::where('status', 'show')->get();

        return $list;
    }

    public static function getCategoryByCode($code)
    {
        return Category::where('code', $code)->first();
    }

    public static function getRightBlockCategories()
    {
        $list = Category::where('options', '&', 2)->get();

        return $list;
    }

}