<?php

namespace App\Repositories;

class PostBaseRepository
{

    public static function priorities()
    {
        return [
            0 => 'Ordinary',
            1 => 'Selected'
        ];
    }

    public static function statuses()
    {
        return [
            'shown' => 'Shown',
            'draft' => 'Draft',
            'archive' => 'Archve',
        ];
    }


}