<?php

namespace App\Repositories;

use App\Category;
use App\Http\Requests\Request;
use App\Post;
use App\Setting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class PostRetrieveRepository extends PostBaseRepository
{

    const count_index_news = 12;
    const count_top_news = 3;
    const post_code_of_news = 'news';
    const post_code_of_online = 'online';

    public function getPaginatedList(Collection $filter, $count_on_page = 12)
    {
        $builder = Post::query();
        if ($filter->count()) {
            $builder->where(function ($query) use ($filter) {
                foreach($filter->toArray() as $type => $filter_type_list) {
                    $method_name = camel_case("implement_{$type}_filter");
                    if (method_exists ( $this, $method_name)) {
                        $this->$method_name($query, $filter_type_list);
                    } else {
                        $this->implementTextFilter($query, $filter_type_list);
                    }
                }
            });
        }
        $builder->active();
        $builder->with(['images', 'tags', 'videos']);
        $builder->orderBy('created_at', 'desc');

        return $builder->paginate($count_on_page);
    }

    protected function implementTextFilter(Builder $query, $data)
    {
        if (!is_array($data)) $data = explode(' ', $data);
        foreach($data as $value) {
            $query->orWhere('title', 'like', '%'.$value.'%');
            $query->orWhere('brief', 'like', '%'.$value.'%');
            $query->orWhere('content', 'like', '%'.$value.'%');
        }
    }

    protected function implementTagFilter(Builder $query, $data)
    {
        $query->orWhereHas('tags', function ($query_tags) use ($data) {
            $query_tags->whereIn('title', $data);
        });
    }

    protected function implementActiveFilter(Builder $query, $data)
    {
        $query->active();
    }

    protected function implementCategoryFilter(Builder $query, $data)
    {
        $query->where('category_id', $data);
    }

    public function prepareListSearch($search_array)
    {
        $result = [];
        foreach($search_array as $search_item) {
            if ($search_item) {
                $result['tag'][] = trim($search_item);
                $result['text'][] = trim($search_item);
            }
        }

        return collect($result);
    }

    public function getIndexList()
    {
        $category = CategoryRepository::getCategoryByCode(self::post_code_of_news);
        if (!$category) {
            return collect([]);
        }
        $list = Post::where('category_id', $category->id)
            ->with(['images', 'videos'])
            ->active()
            ->orderBy('created_at', 'desc')
            ->take(self::count_index_news)
            ->get();

        return $list;
    }

    public function getOnline()
    {
        $category = CategoryRepository::getCategoryByCode(self::post_code_of_online);
        if (!$category) {
            return collect([]);
        }
        $post = Post::where('category_id', $category->id)
            ->with(['images', 'videos'])
            ->where('priority', 1)
            ->active()
            ->orderBy('created_at', 'desc')
            ->first();

        return $post;
    }

    public function isOnlineOn()
    {
        $setting = Setting::where(['key' => 'online'])->first();
        if(!empty($setting->value) && $setting->value == 'on') {
            return true;
        }

        return false;
    }

    public function getCategoriesPostList(Category $category)
    {
        if (!$category) {
            return collect([]);
        }

        return $this->getPaginatedList(collect(['category' => $category->id]));
    }

    public function getOnePost($id)
    {
        $post = Post::active()
            ->where('id', $id)
            ->first();

        return $post;
    }

    public function getRightsLists()
    {
        $result = [];
        $categories = CategoryRepository::getRightBlockCategories();

        while(($category = $categories->shift()) && count($result) < 2) {
            $result[] = [
                'category' => $category,
                'list' => $this->getPaginatedList(collect(['category' => $category->id]), 2)
            ];
        }

        return $result;
    }


}