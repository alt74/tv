<?php

namespace App\Repositories;


use Carbon\Carbon;

class UtilsRepository
{

    public static function getDate(Carbon $date)
    {
        $eng = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $rus = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
        $str = $date->format('d M y, H:i');
        return str_replace($eng, $rus, $str);
    }
}