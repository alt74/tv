<?php

namespace App\Repositories;


class DictionRepository
{
    public static function categoryStatuses()
    {
        return [
            'show' => 'Show in navigation',
            'hide' => 'Hide in navigation'
        ];
    }

    public static function categoryOptions()
    {
        return [
            1 => 'Top menu',
            2 => 'Right block category',
            4 => 'Reserved ...'
        ];
    }

    public static function categoryPriorities()
    {
        return [
            'top'       => 'Top',
            'normal'    => 'Normal',
            'low'       => 'Low'
        ];
    }

}