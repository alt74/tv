<?php

namespace App\Repositories;

use App\Tag;

class TagRepository
{

    public function getTypeaheadSearchList($query)
    {
        $result = [];
        $list = Tag::where('title', 'like', '%' . $query . '%')->get()->toArray();
        foreach($list as $row) {
            $result[] = ['value' => $row['title']];
        }
        return $result;
    }


    public function getIdsByNames($names = [])
    {
        if (empty($names)) {
            return [];
        }
        $names = $this->prepareArray($names);
        return Tag::whereIn('title', $names)->get()
            ->pluck('id')
            ->toArray();
    }

    private function prepareArray($a)
    {
        $result = [];
        if (!is_array($a)) {
            $a = [$a];
        }
        foreach( $a as $item) {
            if (!empty($item)) {
                $result[] = trim($item);
            }
        }
        return $result;
    }

    public function insertNewItems($names = [])
    {
        if (empty($names)) {
            return [];
        }
        $names = $this->prepareArray($names);
        $existed_names =  Tag::whereIn('title', $names)->get()
            ->pluck('title')
            ->toArray();
        $new_names = array_diff($names, $existed_names);
        foreach($new_names as $name) {
            $model = new Tag();
            $model->title = trim($name);
            $model->save();
        }
    }

}