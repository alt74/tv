<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const DATE_FORMAT = 'd.m.Y';
    const TIME_FORMAT = 'H:i';

    protected $fillable = [
        'title', 'brief', 'content',
        'date', 'date_begin', 'date_end',
        'status', 'priority',
        'category_id', 'author_id', 'source_id',
        'date_d', 'date_t',
        'date_begin_d', 'date_begin_t',
        'date_end_d', 'date_end_t',
    ];

    protected $dates = [
        'date', 'date_begin', 'date_end',
        'created_at', 'updated_at'
    ];

    public function tags() {
        return $this->belongsToMany('App\Tag', 'post2tag', 'post_id', 'tag_id');
    }

    public function images() {
        return $this->belongsToMany('App\Image', 'post2image', 'post_id', 'image_id');
    }

    public function videos() {
        return $this->belongsToMany('App\Video', 'post2video', 'post_id', 'video_id');
    }

    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function source()
    {
        return $this->belongsTo('App\Source');
    }


    /*
     * date field attributes
     */
    public function getDateDAttribute()
    {
        return $this->getDateField('date');
    }
    public function setDateDAttribute($v)
    {
        $this->date = $this->setDateField($this->date, $v);
    }
    public function getDateTAttribute()
    {
        return $this->getTimeField('date');
    }
    public function setDateTAttribute($v)
    {
        $this->date = $this->setTimeField($this->date, $v);
    }


    /*
     * date begin field attributes
     */
    public function getDateBeginDAttribute()
    {
        return $this->getDateField('date_begin');
    }
    public function setDateBeginDAttribute($v)
    {
        $this->date_begin = $this->setDateField($this->date_begin, $v);
    }
    public function getDateBeginTAttribute()
    {
        return $this->getTimeField('date_begin');
    }
    public function setDateBeginTAttribute($v)
    {
        $this->date_begin = $this->setTimeField($this->date_begin, $v);
    }

    /*
     * date end field attributes
     */
    public function getDateEndDAttribute()
    {
        return $this->getDateField('date_end', (new Carbon('now'))->modify('5 years')->format(self::DATE_FORMAT));
    }
    public function setDateEndDAttribute($v)
    {
        $this->date_end = $this->setDateField($this->date_end, $v);
    }
    public function getDateEndTAttribute()
    {
        return $this->getTimeField('date_end');
    }
    public function setDateEndTAttribute($v)
    {
        $this->date_end = $this->setTimeField($this->date_end, $v);
    }


    public function getThumbnailAttribute()
    {
        if ($video = $this->videos->shift()) {
            return $video->thumbnail;
        }
        if ($image = $this->images->shift()) {
            return url('/image/'.$image->id.'/thumbnail');
        }
        return url('images/300x200.png');
    }

    public function getMediaAttribute()
    {
        $result = [];
        foreach($this->videos as $video) {
            $result[] = $video->html;
        }
        foreach($this->images as $image) {
            $result[] = $image->html;
        }
        if (count($result) ==0) {
            $result[] = '';
        }

        return $result;
    }

    // ---------------------------------------

    private function getDateField($field_name, $default = '')
    {
        if (empty($this->$field_name)) {
            return $default ?: (new Carbon('now'))->format(self::DATE_FORMAT);
        }
        if ($this->$field_name instanceof Carbon && $this->$field_name->year < 2) {
            return $default;
        }
        return $this->$field_name->format(self::DATE_FORMAT);
    }

    private function getTimeField($field_name)
    {
        if (empty($this->$field_name)) {
            return (new Carbon('now'))->format(self::TIME_FORMAT);
        }
        if ($this->$field_name instanceof Carbon && $this->$field_name->year < 2) {
            return '';
        }
        return $this->$field_name->format(self::TIME_FORMAT);
    }

    private function setDateField($date, $format_string)
    {
        if (empty($date)) {
            $date = new Carbon();
        }
        $v_date = Carbon::createFromFormat('d.m.Y', $format_string);
        if ($v_date) {
            $date->setDate($v_date->year, $v_date->month, $v_date->day);
        }
        return $date;
    }

    private function setTimeField($date, $format_string)
    {
        if (empty($date)) {
            $date = new Carbon();
        }
        $v_date = Carbon::createFromFormat('H:i', $format_string);
        if ($v_date) {
            $date->setTime($v_date->hour, $v_date->minute, 0);
        }
        return $date;
    }

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status', '=', 'shown')
            ->where('date_begin', '<=', date('Y-m-d'))
            ->where('date_end', '>=', date('Y-m-d'))
            ;
    }
}
