<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'title'
    ];

    public function images() {
        return $this->belongsToMany('App\Image', 'image2tag', 'tag_id', 'image_id')->withPivot('status', 'priority');
    }
}
