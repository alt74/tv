<?php

namespace App;

use App\Repositories\ImageRepository;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    protected $fillable = [
        'title',
        'description',
        'position',
    ];

    public function getImageAttribute()
    {
        $repository = new ImageRepository();
        return $repository->getFilePath('image', $this->id);
    }

    public function getThumbnailAttribute()
    {
        $repository = new ImageRepository();
        return $repository->getFilePath('thumbnail', $this->id);
    }


    public function getHtmlAttribute()
    {
        return sprintf('<img src="%s" alt="%s" />', url('/image/'.$this->id), $this->title);
    }


    public function tags() {
        return $this->belongsToMany('App\Tag', 'image2tag', 'image_id', 'tag_id')->withPivot('status', 'priority');
    }


}
