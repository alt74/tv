<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'title', 'code', 'description', 'status', 'priority', 'options', 'options_array'
    ];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }


    public function setOptionsArrayAttribute($v)
    {
        $this->options = $this->convertArrayOptions2Int($v);
    }

    private function convertArrayOptions2Int($v)
    {
        $result = 0;
        if(is_array($v)) {
            foreach($v as $i) {
                $result = $result | $i;
            }
        }

        return $result;
    }

}
