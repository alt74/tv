<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'title',
        'description',
        'url',
        'info'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function tags() {
        return $this->belongsToMany('App\Tag', 'video2tag', 'video_id', 'tag_id')->withPivot('status', 'priority');
    }

    public function getDataAttribute()
    {
        $data = [];
        try {
            $data = \GuzzleHttp\json_decode($this->info, true);
            if (is_array($data)) {
                $data = array_values($data)[0];
            }
        } catch (\Exception $e) {

        }
        return $data;
    }

    public function getThumbnailAttribute()
    {
        return isset($this->data['thumbnail_url']) ? $this->data['thumbnail_url'] : 'http://via.placeholder.com/300x200';
    }

    public function getHtmlAttribute()
    {
        return isset($this->data['html']) ? $this->data['html'] : '';
    }

}
